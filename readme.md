# Tepelná závislost senzorů 

## Kódy

Soubory které generují grafy:
1. "data_processing.ipynb" - všechno až na variace a grafy v "popis problému"
2. "raw_data_graphs.ipynb" - grafy v "popis problému"
3. "variace.ipynb" - variance


## Popis problému

Jako bakalářskou práci jsem navrhl z zkonstruoval letový počítač, určený do modelové rakety s dostupem přibližně 600m. Protože se naskytla situace, byl tento počítač přidělaný na meteorologický balón od ČHMÚ.

Parametry letu zaznamenávala dvojice tlakových a teplotních senzorů - senzor A na letovém počítači (měl by být kvalitnější) a externí senzor B (levnější) poskytnutý od ČHMÚ, který byl vystavený vnějším podmínkám.
Ani jeden ze senzorů nebyl kalibrovaný, tudíž není neznám "ground truth" a ani nejsem schopen naměřená data kompenzovat.

Graf tlaku po dobu letu:

![image info](./Data/press_fc.png)

Graf teplot po dobu letu:

![image info](./Data/temps.png)

Proběhl let, ve kterém počítač vylétl až do výšky s nejmenším naměřeným tlakem 592 Pa pomocí senzoru A, 599 Pa pomocí senzoru B.
## Hypotéza

Protože nemůžu porovnávat kvalitu měření bez kalibrace, je možné alespoň určit zda a případně v jaké míře ovlivňuje teplota senzoru jeho měření? Pomáhá ohřev senzoru pro minimalizaci chyby?

## Postup
Mám dva sety dat:
1. Tlak a teplota v závislosti na čase pro sensor A (byl vyhříván v průběhu letu)
2. Tlak a teplota v závislosti na čase pro sensor B (nebyl nijak vyhříván)

Dle atmosférických modelů tlak v atmosféře ubývá exponenciálně s výškou.
Protože balón po krátké chvíli dosáhne terminální rychlosti, dá se jeho rychlost stoupání považovat za cca lineární, tudíž i tlak by s časem měl exponenciálně ubývat.

Pro jednoduchost se budu zabývat jen letem nahoru, jelikož let dolů by měl vykazovat prakticky identické chování co se teplot týče.

Proto každý set dat proložím samostatně exponenciálou. 

Protože očekávám že naměřený tlak bude driftovat s teplotou senzorů, vypočítám korelaci residuí regrese (tj. chyba od predikovaného hladkého průběhu) a teploty.

Jako bonus je možné vypočítat rozptyl hodnot senzorů na zemi.
## Výsledky

Objevil jsem, že existuje několik variant jak vynášet exponenciální data.
Prvním z nich je linearizace exponenciály inverzní funkcí, tudíž logaritmem. Na takto linearizovaná data bude následně aplikována standardní lineární regrese a data opět dosazeny do exponenciály, stejně jak získané koeficienty. Tento postup má problém, jelikož velké hodnoty dáte do stejné úrovně jak malé, budou mít původně malé hodnoty stejný vliv na výslednou křivku jak původně velké hodnoty, tudíž výsledný fit ve větších hodnotách bude horší jak u malých. Takto pracuje standardní polyfit funkce.

Tento problém se dá vyřešit váhováním, při kterém se větším hodnotám přiřadí větší váha ("weighted regression").

Nejlepším regresním způsobem ovšem vůbec data nelinearizovat, ale snažit se jednoduše minimalizovat rozdíly čtverců nějaké poskytnuté funkce na setu dat. Je patrné, že tento postup je podstatně výpočetně náročnější. Další nevýhodou je, že požaduje počáteční podmínku od které postupně iteruje k nejlepšímu řešení, tudíž závisí na její volbě. Ale velká výhoda je, že tato funkce podporuje vertikální offset, oproti polyfit. Takto funguje curve_fit funkce.

Jednotlivé fity a jejich residua na jdou vidět na následujících grafech:
Sensor A:

![image info](./Data/press_fc_asc.png)


```
INTERNAL SENSOR
rounded results:

ASC R_squared linear (polyfit w/o weight):0.97
ASC linear (polyfit w/o weight) eq: 1153*e(-0.0528*t)
ASC R_squared linear (polyfit with weight):0.99622
ASC linear (polyfit with weight) eq: 1022*e(-0.0488*t)
ASC R_squared nonlinear (curve_fit):0.99897
ASC nonlinear (curve_fit) eq: -17 + 985*e(-0.04337*t)
```

![image info](./Data/press_fc_asc_pred_diff.png)

Sensor B:

![image info](./Data/press_ext_asc.png)

```
EXTERNAL SENSOR
rounded results:

ASC R_squared linear (polyfit w/o weight):0.982
ASC linear (polyfit w/o weight) eq: 1114*e(-0.0518*t) 
ASC R_squared linear (polyfit with weight):0.99649
ASC linear (polyfit with weight) eq: 1017*e(-0.0487*t) 
ASC R_squared nonlinear (curve_fit):0.99896
ASC linear (polyfit with weight) eq: -16 + 982*e(-0.04361*t)
```

![image info](./Data/press_ext_asc_pred_diff.png)

Na porovnání a výpočet korelace jsem zvolil residua od nejlepšího fitu, tudíž curve_fit.
Je vidět že při porovnání teplotních grafů a residuí je korelace pravděpodobná:

Sensor A:

```
Korelační koeficient:  0.182
```

![image info](./Data/press_fc_asc_corr.png)

Sensor B:

```
Korelační koeficient:  0.302
```

![image info](./Data/press_ext_asc_corr.png)


Variace:

Z prvního stand-by jsem vypočítal pro oba senzory varianci. To slouží jako náhled na obecný šum senzoru, nicméně je docela rozumné očekávat že v průběhu změny tlaku se bude velikost šumu měnit, tudíž to není možné použít jako metrika.

```
Variance A, internal:76.45 Pa, 0.07% of normal value
Variance B, external:150.94 Pa, 0.15% of normal value
```
Je vidět že rozptyl u lepšího senzoru A je poloviční oproti senzoru B.

Data po přistání jsem ořezal až do místa, kdy se tlak začal měnit (= kluci našli modul a jeli s ním autem domů i když stále zaznamenával).

```
Variance FC:287.8488 Pa, 0.29% of normal value
Variance External:537.9691 Pa, 0.55% of normal value
```

# Zhodnocení výsledků

Při pohledu na výsledky, je zřejmé že curve_fit funkce výrazně lépe vystihuje povahu dat. Vypsané rovnice mají následující význam:
1. v lineárním případě: (průsečík s osou y)*e(časový koeficient * čas)
2. v nelineárním curve_fit případě: (hodnota v nekonečnu) + (průsečík s osou y)*e(časový koeficient * čas).

Korelační koeficient 0,182 v případě senzoru A znamená mírnou závislost naměřeného tlaku na teplotě. V tomto případě byl senzor spolu s interní elektronikou vyhříván, tudíž teplotní rozdíly nejsou tak drastické jak v případě B.

Stejně jak v následujícím případě, prvotní velký "dip", pro který nemám vysvětlení, velmi pravděpodobně značně potlačuje vypočítanou korelaci.

V případě B, korelační koeficient 0.302 vyjadřuje značnou závislost. Je vidět, že vnější senzor, který byl vystaven většímu intervalu teplot, vykazuje větší korelaci.

Při pohledu na residua si lze ovšem všimnout že až na zvlnění jsou prakticky totožná. Proto si myslím že tato korelace vznikla pouze díky většímu rozpětí teploty, nikoli že by residua byla tolikrát více ovlivněna teplotou. Ovšem při pohledu na zvlnění v grafu pro sensor B je vidět, že krátké propady v teplotě a residuích se přesně překrývají.

Graf teplotní závislosti tlaku senzoru A z datasheetu. Teplotní závislost pro senzor B, kde je problém aktuálnější a s větší korelace, se nepodařilo najít. 

```
1 mbar = 100 Pa
```
<img src="./Data/PressureErrorVsTempFCAbs.png" alt= “” width="700px" height="400px">

Ačkoliv se jedná o ten údajně kvalitnější senzor, je vidět že absolutní naměřený tlak je při malých okolních tlacích velmi ovlivňován teplotou (v řádů 100 Pa). Je rozumné očekávat že když datasheet senzoru B tento graf neposkytl, bude tato závislost pravděpodobně větší.

Je vidět že ačkoliv variace stále udržují přibližný poměr 1:2, rozptyl je přibližně 5x tak velký jak v prvním případě.
Při pohledu na data, lze zjistit že tlaky v obou případech pomalu "driftují" nahoru, ačkoliv modul ležel na místě. Jediné odůvodnění mě napadá, že senzory jak byly vystaveny tlaku nižšímu než na jaký jsou konstruované, postupně "driftují" zpět do normálního stavu.

# Závěr
Usuzuji, že naměřený tlak je relativně lehce zkreslován teplotou senzoru.  Při pohledu na zvlnění v grafu pro sensor B je vidět, že krátké propady v teplotě a residuích se přesně překrývají, proto soudím že teplota má na tlak vliv. Tato hypotéza je dále potvrzena alespoň grafem tlaku v závislosti na teplotě senzoru A, když graf pro sensor B není dostupný. Protože u senzoru A se takové propady nevyskytují, je rozumné usuzovat že stabilita a přesnost výstupních dat senzoru se dá zvýšit umístěním senzoru na hmotný objekt jako "tepelný kondenzátor" na pokrytí tepelných výkyvů, resp. vyhřívání senzoru na stanovenou teplotu pro co minimální rozkmit teplot během letu.